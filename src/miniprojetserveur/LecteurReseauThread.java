/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miniprojetserveur;

import java.io.IOException;
import java.net.Socket;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author arthurdambrine
 */
public class LecteurReseauThread extends Thread {

    private String message;
    private IOCommandes io;
    private Socket socket;

    public LecteurReseauThread(Socket socket) {
        this.socket = socket;
        start();
    }

    @Override
    public void run() {
        String s = "";
        try {
            io = new IOCommandes(this.socket);
        } catch (IOException ex) {
            Logger.getLogger(LecteurReseauThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Lecture sur le reseau dans le thread.
        boolean b = true;
        while (b) {
            try {
                try {
                    s = io.lireReseau();
                    if(s.equals("/quit")){
                        b = false;
                    }
                    io.ecrireEcran(s);
                } catch (IOException ex) {
                    Logger.getLogger(LecteurReseauThread.class.getName()).log(Level.SEVERE, null, ex);
                }
                Thread.sleep(1000); //attend 1 seconde
            } catch (InterruptedException e) {
            }
        }
    }
}
